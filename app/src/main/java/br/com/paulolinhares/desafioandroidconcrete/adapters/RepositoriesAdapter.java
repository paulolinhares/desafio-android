package br.com.paulolinhares.desafioandroidconcrete.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.paulolinhares.desafioandroidconcrete.PullRequestListActivity;
import br.com.paulolinhares.desafioandroidconcrete.R;
import br.com.paulolinhares.desafioandroidconcrete.databinding.RowRepositoryBinding;
import br.com.paulolinhares.desafioandroidconcrete.models.Item;
import br.com.paulolinhares.desafioandroidconcrete.utils.Const;
import br.com.paulolinhares.desafioandroidconcrete.view_model.RepositoryViewModel;

/**
 * Created by paulolinhares on 11/10/17.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.AdapterViewHolder> {
    private static final String TAG = RepositoriesAdapter.class.getName();

    List<Item> listItems;
    Context context;
    private RowRepositoryBinding mBinding;

    public RepositoriesAdapter(Context context) {
        this.context = context;
    }

    public RepositoriesAdapter(List<Item> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public AdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        mBinding = DataBindingUtil.inflate(inflater, R.layout.row_repository, viewGroup, false);

        return new AdapterViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(AdapterViewHolder viewHolder, int i) {
        Item item = listItems.get(i);
        Item.Owner owner = item.getOwner();
        RepositoryViewModel repository = new RepositoryViewModel(item.getName(), item.getDescription(), item.getForks(), item.getStargazersCount(), owner.getLogin(), owner.getAvatarUrl());

        //set widgets
        viewHolder.bind(repository);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public void setItems(List<Item> listItems) {
        this.listItems = listItems;
    }

    public class AdapterViewHolder extends RecyclerView.ViewHolder {
        RepositoryViewModel repository;
        public AdapterViewHolder(final RowRepositoryBinding mBinding) {
            super(mBinding.getRoot());

            mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openPullRequestsActivity(mBinding);
                }
            });
        }

        private void openPullRequestsActivity(RowRepositoryBinding mBinding) {
            Intent intent = new Intent(context, PullRequestListActivity.class);
            intent.putExtras(prepereBundle());
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, mBinding.cvItem, "imageAvatar");
            context.startActivity(intent, options.toBundle());
        }

        public void bind(RepositoryViewModel repository){
            this.repository = repository;
            mBinding.setRepository(repository);
        }

        private Bundle prepereBundle() {
            Bundle bundle = new Bundle();
            bundle.putString(Const.Params.creator, repository.getLogin());
            bundle.putString(Const.Params.repository, repository.getName());
            bundle.putString(Const.Params.avatar_url, repository.getAvatarUrl());

            return bundle;
        }


    }
}
