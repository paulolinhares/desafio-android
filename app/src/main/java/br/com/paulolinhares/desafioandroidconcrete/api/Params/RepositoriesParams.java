package br.com.paulolinhares.desafioandroidconcrete.api.Params;

import android.os.Parcel;
import android.os.Parcelable;

import br.com.paulolinhares.desafioandroidconcrete.utils.Const;

/**
 * Created by paulolinhares on 11/10/17.
 */

public class RepositoriesParams implements Parcelable {
    public String q;
    public String sort;
    public int page;

    public RepositoriesParams() {
        this.q       = Const.repositoryName;
        this.sort    = Const.reposirorySort;
        this.page    = 1;
    }

    protected RepositoriesParams(Parcel in) {
        q = in.readString();
        sort = in.readString();
        page = in.readInt();
    }

    public static final Creator<RepositoriesParams> CREATOR = new Creator<RepositoriesParams>() {
        @Override
        public RepositoriesParams createFromParcel(Parcel in) {
            return new RepositoriesParams(in);
        }

        @Override
        public RepositoriesParams[] newArray(int size) {
            return new RepositoriesParams[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(q);
        parcel.writeString(sort);
        parcel.writeInt(page);
    }
}
