package br.com.paulolinhares.desafioandroidconcrete;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;
import java.util.List;

import br.com.paulolinhares.desafioandroidconcrete.adapters.RepositoriesAdapter;
import br.com.paulolinhares.desafioandroidconcrete.api.Params.RepositoriesParams;
import br.com.paulolinhares.desafioandroidconcrete.api.Routes;
import br.com.paulolinhares.desafioandroidconcrete.api.wrappers.WrapperRepositories;
import br.com.paulolinhares.desafioandroidconcrete.controllers.ControllerRetrofit;
import br.com.paulolinhares.desafioandroidconcrete.databinding.ActivityMainBinding;
import br.com.paulolinhares.desafioandroidconcrete.models.Item;
import br.com.paulolinhares.desafioandroidconcrete.utils.Const;
import br.com.paulolinhares.desafioandroidconcrete.utils.EndlessRecyclerOnScrollListener;
import br.com.paulolinhares.desafioandroidconcrete.utils.LogUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding mBinding;
    private static final String TAG = MainActivity.class.getName();
    private List<Item> listItems = new ArrayList<>();
    private RecyclerView rvRepositories;
    RepositoriesAdapter adapter;
    private RepositoriesParams params;
    private LinearLayoutManager linearLayoutManager;
    private Parcelable listState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setSupportActionBar(mBinding.toolbar);

        //sets data binding
        mBinding.setView(this);

        //Config RecycleView
        configRecycleView();

        //set Adapter
        setRecycleViewWithData();

        //Get repositories on API GitHub
        if (savedInstanceState == null) {
            params = new RepositoriesParams();
            getRepositories(params);
        }

        //endless Scrolling
        endlessScrolling();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Const.Params.repositories_params, params);
        outState.putParcelableArrayList(Const.Params.repositories_list, (ArrayList<? extends Parcelable>) listItems);
        outState.putParcelable(Const.Params.list_state, rvRepositories.getLayoutManager().onSaveInstanceState());
        outState.putInt(Const.Params.fab_state, mBinding.fab.getVisibility());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null){
            params = savedInstanceState.getParcelable(Const.Params.repositories_params);
            listItems = savedInstanceState.getParcelableArrayList(Const.Params.repositories_list);
            listState = savedInstanceState.getParcelable(Const.Params.list_state);

            //manager fab state
            managerFab(savedInstanceState.getInt(Const.Params.fab_state));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(listState != null) {//screen was rotated
                adapter.setItems(listItems);
                rvRepositories.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    private void configRecycleView() {
        rvRepositories = mBinding.rvRepositories;
        linearLayoutManager = new LinearLayoutManager(this);
        rvRepositories.setLayoutManager(linearLayoutManager);
    }

    private void getRepositories(final RepositoriesParams params) {
        managerProgress(params, View.VISIBLE);

        Retrofit retrofit = ControllerRetrofit.initializer();
        Routes routes = retrofit.create(Routes.class);
        Call<WrapperRepositories> call = routes.getRespositories(params.q, params.sort, params.page);
        call.enqueue(new Callback<WrapperRepositories>() {
            @Override
            public void onResponse(@NonNull Call<WrapperRepositories> call, @NonNull Response<WrapperRepositories> response) {
                LogUtil.i(TAG, "response: " + response.code());
                managerProgress(params, View.GONE);
                if (response.code() == 200){
                    WrapperRepositories wr = response.body();
                    if (wr != null){
                        if (params.page == 1) {
                            listItems = wr.getItems();
                            //set Adapter in RecycleView
                            setRecycleViewWithData();
                        }else{
                            listItems.addAll(wr.getItems());
                            adapter.notifyDataSetChanged();
                            managerFab(View.VISIBLE);
                        }
                    }
                }else{
                    //TODO view error
                }
            }

            @Override
            public void onFailure(@NonNull Call<WrapperRepositories> call, @NonNull Throwable t) {
                mBinding.flProgress.setVisibility(View.GONE);
                LogUtil.e(TAG, "getRepositories: " + t.getMessage());
            }
        });

    }

    private void managerFab(int showDone) {
        Animation animation;
        if (showDone == View.VISIBLE) {
            animation = AnimationUtils.loadAnimation(this, R.anim.fab_animation_show);
        }else{
            animation = AnimationUtils.loadAnimation(this, R.anim.fab_animation_done);
        }

        mBinding.fab.setVisibility(showDone);
        mBinding.fab.setAnimation(animation);
    }

    private void managerProgress(RepositoriesParams params, int showDone) {
        if (params.page == 1)
            mBinding.flProgress.setVisibility(showDone);
        else
            mBinding.loadEndless.setVisibility(showDone);
    }

    private void endlessScrolling() {
        rvRepositories.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                LogUtil.i(TAG, "currentPage: " + current_page);
                params.page = 2;
                getRepositories(params);
            }
        });
    }

    private void setRecycleViewWithData() {
        adapter = new RepositoriesAdapter(listItems, MainActivity.this);
        rvRepositories.setAdapter(adapter);
    }

    public void clickFloatingActionButtom(View view) {
        Snackbar.make(view, getResources().getString(R.string.return_to_top), Snackbar.LENGTH_LONG)
                .show();
        rvRepositories.smoothScrollToPosition(0);
        managerFab(View.GONE);
    }
}
