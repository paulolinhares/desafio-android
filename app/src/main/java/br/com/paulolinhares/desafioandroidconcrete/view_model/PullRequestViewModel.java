package br.com.paulolinhares.desafioandroidconcrete.view_model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.paulolinhares.desafioandroidconcrete.BR;
import br.com.paulolinhares.desafioandroidconcrete.R;
import br.com.paulolinhares.desafioandroidconcrete.utils.ControllerText;

/**
 * Created by paulolinhares on 11/10/17.
 */

public class PullRequestViewModel extends BaseObservable {
    public String title;
    public String body;
    public String created_at;

    public String login;
    public String avatar_url;

    public PullRequestViewModel(String title, String body, String created_at, String login, String avatar_url) {
        this.title = title;
        this.body = body;
        this.created_at = created_at;
        this.login = login;
        this.avatar_url = avatar_url;
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
        notifyPropertyChanged(BR.body);
    }

    @Bindable
    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
        notifyPropertyChanged(BR.created_at);
    }

    @Bindable
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
        notifyPropertyChanged(BR.login);
    }

    @Bindable
    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
        notifyPropertyChanged(BR.avatar_url);
    }

    @BindingAdapter("bind:imageUrl")
    public static void loadImageAvatar(ImageView imageView, String avatar_url){
        Picasso.with(imageView.getContext())
                .load(avatar_url)
                .placeholder(R.drawable.ic_person_outline_24dp)
                .into(imageView);
    }

    @BindingAdapter("bind:text")
    public static void sorthenText(TextView textView, String text){
        if (textView.getId() == R.id.tvDescription)
            textView.setText(ControllerText.shortenText(text, 30));
    }
}
