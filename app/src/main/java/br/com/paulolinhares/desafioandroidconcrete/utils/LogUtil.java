package br.com.paulolinhares.desafioandroidconcrete.utils;

import android.util.Log;

/**
 * Created by paulolinhares on 11/10/17.
 */

public class LogUtil {
    private static boolean enable = true;

    public static void i(String tag, String message){
        if (enable)
            Log.i(tag, message);
    }

    public static void e(String tag, String message){
        if (enable)
            Log.e(tag, message);
    }


}
