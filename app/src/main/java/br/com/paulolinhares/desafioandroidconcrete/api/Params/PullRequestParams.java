package br.com.paulolinhares.desafioandroidconcrete.api.Params;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by paulolinhares on 11/10/17.
 */

public class PullRequestParams implements Parcelable {
    public String creator;
    public String repository;

    public PullRequestParams(String creator, String repository) {
        this.creator = creator;
        this.repository = repository;
    }

    protected PullRequestParams(Parcel in) {
        creator = in.readString();
        repository = in.readString();
    }

    public static final Creator<PullRequestParams> CREATOR = new Creator<PullRequestParams>() {
        @Override
        public PullRequestParams createFromParcel(Parcel in) {
            return new PullRequestParams(in);
        }

        @Override
        public PullRequestParams[] newArray(int size) {
            return new PullRequestParams[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(creator);
        parcel.writeString(repository);
    }
}
