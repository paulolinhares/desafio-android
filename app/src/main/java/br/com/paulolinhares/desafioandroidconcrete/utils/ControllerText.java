package br.com.paulolinhares.desafioandroidconcrete.utils;

/**
 * Created by paulolinhares on 14/10/17.
 */

public class ControllerText {

    public static String shortenText(String text, int words){
        if (text == null)
            return "...";
        String[] wordsFromText = text.split(" ");
        if (wordsFromText.length > words){
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < words; i++){
                sb.append(wordsFromText[i]);
                sb.append(" ");
            }
            sb.append("...");
            return sb.toString();
        }

        return text;
    }
}
