package br.com.paulolinhares.desafioandroidconcrete.api.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by paulolinhares on 11/10/17.
 */

public class WrapperPullRequest implements Parcelable {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    protected WrapperPullRequest(Parcel in) {
        title = in.readString();
        body = in.readString();
        created_at = in.readString();
    }

    public static final Creator<WrapperPullRequest> CREATOR = new Creator<WrapperPullRequest>() {
        @Override
        public WrapperPullRequest createFromParcel(Parcel in) {
            return new WrapperPullRequest(in);
        }

        @Override
        public WrapperPullRequest[] newArray(int size) {
            return new WrapperPullRequest[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeString(created_at);
    }

    //Inner class User
    public static class User implements Parcelable {
        @SerializedName("login")
        @Expose
        private String login;

        @SerializedName("avatar_url")
        @Expose
        private String avatarUrl;

        protected User(Parcel in) {
            login = in.readString();
            avatarUrl = in.readString();
        }

        public static final Creator<User> CREATOR = new Creator<User>() {
            @Override
            public User createFromParcel(Parcel in) {
                return new User(in);
            }

            @Override
            public User[] newArray(int size) {
                return new User[size];
            }
        };

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(login);
            parcel.writeString(avatarUrl);
        }
    }
}
