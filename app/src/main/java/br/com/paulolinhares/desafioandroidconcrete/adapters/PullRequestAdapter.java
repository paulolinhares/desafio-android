package br.com.paulolinhares.desafioandroidconcrete.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.paulolinhares.desafioandroidconcrete.R;
import br.com.paulolinhares.desafioandroidconcrete.api.wrappers.WrapperPullRequest;
import br.com.paulolinhares.desafioandroidconcrete.databinding.RowPullRequestBinding;
import br.com.paulolinhares.desafioandroidconcrete.utils.ControllerText;
import br.com.paulolinhares.desafioandroidconcrete.view_model.PullRequestViewModel;

/**
 * Created by paulolinhares on 11/10/17.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.AdapterViewHolder> {
    private static final String TAG = PullRequestAdapter.class.getName();

    List<WrapperPullRequest> listItems;
    Context context;
    private RowPullRequestBinding mBinding;
    private PullRequestViewModel pullRequest;


    public PullRequestAdapter(List<WrapperPullRequest> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @Override
    public AdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        mBinding = DataBindingUtil.inflate(inflater, R.layout.row_pull_request, viewGroup, false);

        return new AdapterViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(AdapterViewHolder viewHolder, int i) {
        WrapperPullRequest item = listItems.get(i);
        WrapperPullRequest.User user = item.getUser();

        pullRequest = new PullRequestViewModel(item.getTitle(), item.getBody(), item.getCreated_at(), user.getLogin(), user.getAvatarUrl());

        //set widgets
        viewHolder.bind(pullRequest);
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public void setPulls(List<WrapperPullRequest> listPullRequest) {
        this.listItems = listPullRequest;
    }

    public class AdapterViewHolder extends RecyclerView.ViewHolder {

        public AdapterViewHolder(final RowPullRequestBinding mBinding) {
            super(mBinding.getRoot());

            mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String textCurrentLenght = mBinding.tvDescription.getText().toString();
                    if(textCurrentLenght.length() < 150)
                        mBinding.tvDescription.setText(pullRequest.body);
                    else
                        mBinding.tvDescription.setText(ControllerText.shortenText(pullRequest.body, 5));
                }
            });
        }

        public void bind(PullRequestViewModel item){
            mBinding.setPullrequest(item);
            mBinding.tvName.setText(item.title);
        }


    }
}
