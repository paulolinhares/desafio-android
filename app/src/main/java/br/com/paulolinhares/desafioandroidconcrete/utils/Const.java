package br.com.paulolinhares.desafioandroidconcrete.utils;

/**
 * Created by paulolinhares on 11/10/17.
 */

public class Const {
    public static final String BASE_URL = "https://api.github.com/";
    public static String repositoryName = "language:java";
    public static String reposirorySort = "stars";

    public static class Params {
        public static String creator = "creator";
        public static String repository = "repository";
        public static String repositories_params = "repositories_params";
        public static String repositories_list = "repositories_list";
        public static String list_state = "list_state";
        public static String fab_state = "fab_state";
        public static String pull_request_params = "pull_request_params";
        public static String pull_request_list = "pull_request_list";
        public static String avatar_url = "avatar_url";
    }
}
