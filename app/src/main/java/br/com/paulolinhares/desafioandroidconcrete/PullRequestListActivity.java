package br.com.paulolinhares.desafioandroidconcrete;

import android.databinding.DataBindingUtil;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.paulolinhares.desafioandroidconcrete.adapters.PullRequestAdapter;
import br.com.paulolinhares.desafioandroidconcrete.api.Params.PullRequestParams;
import br.com.paulolinhares.desafioandroidconcrete.api.Routes;
import br.com.paulolinhares.desafioandroidconcrete.api.wrappers.WrapperPullRequest;
import br.com.paulolinhares.desafioandroidconcrete.controllers.ControllerRetrofit;
import br.com.paulolinhares.desafioandroidconcrete.databinding.ActivityPullRequestListBinding;
import br.com.paulolinhares.desafioandroidconcrete.utils.Const;
import br.com.paulolinhares.desafioandroidconcrete.utils.LogUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PullRequestListActivity extends AppCompatActivity {
    ActivityPullRequestListBinding mBinding;

    private static final String TAG = PullRequestListActivity.class.getName();
    private List<WrapperPullRequest> listPullRequest = new ArrayList<>();
    private RecyclerView rvPullRequest;
    PullRequestAdapter adapter;
    private PullRequestParams params;
    private Parcelable stateList;
    private ActionBar actioBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_pull_request_list);
        mBinding.setView(this);

        rvPullRequest = mBinding.rvPullRequest;

        //set Toolbar
        configToolbar();

        //get Pull Requests
        params = prepereParamsFromGetIntent();

        //Config RecycleView
        configRecycleView();
        //set init RecycleView
        setRecycleViewWithData();

        //Get Data from API
        if (savedInstanceState == null)//first time here
            getPullRequests(params);

    }

    private void configToolbar() {
        actioBar = getSupportActionBar();
        assert actioBar != null;
        actioBar.setDisplayHomeAsUpEnabled(true);
        actioBar.setDisplayShowHomeEnabled(false);
        actioBar.setDisplayUseLogoEnabled(false);
        actioBar.setDisplayShowCustomEnabled(true);
        actioBar.setCustomView(R.layout.toolbar_custom);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Const.Params.pull_request_params, params);
        outState.putParcelableArrayList(Const.Params.pull_request_list, (ArrayList<? extends Parcelable>) listPullRequest);
        outState.putParcelable(Const.Params.list_state, rvPullRequest.getLayoutManager().onSaveInstanceState());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        params = savedInstanceState.getParcelable(Const.Params.pull_request_params);
        listPullRequest = savedInstanceState.getParcelableArrayList(Const.Params.pull_request_list);
        stateList = savedInstanceState.getParcelable(Const.Params.list_state);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (stateList != null) {//screen was rotated
            adapter.setPulls(listPullRequest);
            rvPullRequest.getLayoutManager().onRestoreInstanceState(stateList);

        }
    }

    private void configRecycleView() {
        rvPullRequest = mBinding.rvPullRequest;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvPullRequest.setLayoutManager(linearLayoutManager);
    }

    private PullRequestParams prepereParamsFromGetIntent() {
        String creator;
        String repository;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            creator = bundle.getString(Const.Params.creator);
            repository = bundle.getString(Const.Params.repository);
            PullRequestParams params = new PullRequestParams(creator, repository);
            String imageAvatarUrl = bundle.getString(Const.Params.avatar_url);

            //get Avatar too
            loadImageAvatarInToolbar(imageAvatarUrl);
            setTitleInToolbar(repository);

            return params;
        }

        return null;
    }

    private void setTitleInToolbar(String repository) {
        TextView tvTitleToolbar = actioBar.getCustomView().findViewById(R.id.tvTitleTollbar);
        tvTitleToolbar.setText(repository);
    }



    private void getPullRequests(PullRequestParams params) {
        if (params == null) {
            //TODO show error no params
            return;
        }
        mBinding.flProgress.setVisibility(View.VISIBLE);
        Retrofit retrofit = ControllerRetrofit.initializer();
        Routes routes = retrofit.create(Routes.class);
        Call<List<WrapperPullRequest>> call = routes.getPullRequests(params.creator, params.repository);
        call.enqueue(new Callback<List<WrapperPullRequest>>() {
            @Override
            public void onResponse(@NonNull Call<List<WrapperPullRequest>> call, @NonNull Response<List<WrapperPullRequest>> response) {
                LogUtil.i(TAG, "response: " + response.code());
                mBinding.flProgress.setVisibility(View.GONE);

                if (response.code() == 200){
                    listPullRequest = response.body();
                    if (listPullRequest != null){
                        //set Adapter in RecycleView
                        setRecycleViewWithData();
                    }
                }else{
                    //TODO view error
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<WrapperPullRequest>> call, @NonNull Throwable t) {
                mBinding.flProgress.setVisibility(View.GONE);
                LogUtil.e(TAG, "getRepositories: " + t.getMessage());
            }
        });

    }

    private void loadImageAvatarInToolbar(String imageAvatarUrl) {
        ImageView ivLogoToolbar = actioBar.getCustomView().findViewById(R.id.ivLogoToolbar);
        Picasso.with(this).load(imageAvatarUrl).into(ivLogoToolbar);
    }

    private void setRecycleViewWithData() {
        adapter = new PullRequestAdapter(listPullRequest, PullRequestListActivity.this);
        rvPullRequest.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
