package br.com.paulolinhares.desafioandroidconcrete.api;

import java.util.List;

import br.com.paulolinhares.desafioandroidconcrete.api.wrappers.WrapperPullRequest;
import br.com.paulolinhares.desafioandroidconcrete.api.wrappers.WrapperRepositories;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by paulolinhares on 11/10/17.
 */

public interface Routes {

    //Get all repositories by name type "q"
    @GET("search/repositories")
    Call<WrapperRepositories> getRespositories(
            @Query("q") String language,
            @Query("sort") String sort,
            @Query("page") int page);


    @GET("repos/{creator}/{repository}/pulls")
    Call<List<WrapperPullRequest>> getPullRequests(@Path("creator") String creator, @Path("repository") String repository);
}
