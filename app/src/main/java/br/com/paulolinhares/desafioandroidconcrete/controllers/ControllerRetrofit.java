package br.com.paulolinhares.desafioandroidconcrete.controllers;

import br.com.paulolinhares.desafioandroidconcrete.utils.Const;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by paulolinhares on 11/10/17.
 */

public class ControllerRetrofit {

    public static Retrofit initializer(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Const.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
