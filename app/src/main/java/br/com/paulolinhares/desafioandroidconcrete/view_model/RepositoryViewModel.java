package br.com.paulolinhares.desafioandroidconcrete.view_model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.paulolinhares.desafioandroidconcrete.BR;
import br.com.paulolinhares.desafioandroidconcrete.R;
import br.com.paulolinhares.desafioandroidconcrete.utils.ControllerText;

/**
 * Created by paulolinhares on 11/10/17.
 */

public class RepositoryViewModel extends BaseObservable {
    public String name;
    public String description;
    public int forks;
    public int stargazersCount;
    public String login;
    public String avatarUrl;

    public RepositoryViewModel(String name, String description, int forks, int stargazersCount, String login, String avatarUrl) {
        this.name = name;
        this.description = description;
        this.forks = forks;
        this.stargazersCount = stargazersCount;
        this.login = login;
        this.avatarUrl = avatarUrl;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }

    @Bindable
    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
        notifyPropertyChanged(BR.forks);
    }

    @Bindable
    public int getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(int stargazersCount) {
        this.stargazersCount = stargazersCount;
        notifyPropertyChanged(BR.stargazersCount);
    }

    @Bindable
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
        notifyPropertyChanged(BR.login);
    }

    @Bindable
    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        notifyPropertyChanged(BR.avatar_url);
        //loadImage(BR.ivPhoto, avatarUrl);
    }

    @BindingAdapter("bind:imageUrl")
    public static void loadImage(ImageView view, String avatarUrl) {
        Picasso.with(view.getContext())
                .load(avatarUrl)
                .placeholder(R.drawable.ic_person_outline_24dp)
                .into(view);
    }

    @BindingAdapter("bind:text")
    public static void sorthenText(TextView textView, String text){
        if (textView.getId() == R.id.tvDescription)
            textView.setText(ControllerText.shortenText(text, 15));
    }
}
