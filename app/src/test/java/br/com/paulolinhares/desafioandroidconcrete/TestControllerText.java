package br.com.paulolinhares.desafioandroidconcrete;

import org.junit.Test;

import br.com.paulolinhares.desafioandroidconcrete.utils.ControllerText;

import static org.junit.Assert.assertEquals;

/**
 * Created by paulolinhares on 14/10/17.
 */

public class TestControllerText {


    @Test
    public void testShortenText() throws Exception{
        int wordsInTextFinal = 5;
        String textShorten = ControllerText.shortenText("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make", wordsInTextFinal);
        assertEquals(wordsInTextFinal + 1, textShorten.split(" ").length); //+ 1 porque é adicionado a String "..." no final do text encurtado.
    }
}
